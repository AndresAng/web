CREATE TABLE PRODUCTO(
COD_PRODUCTO INTEGER NOT NULL PRIMARY KEY,
NOM_PRODUCTO VARCHAR(100) NOT NULL,
TIPO VARCHAR(50) NOT NULL,
MARCA VARCHAR(50) NOT NULL,
CANTIDAD INTEGER NOT NULL,
CAPACIDAD NUMERIC(4,2),
DIMENSIONES VARCHAR(20),
RAM INTEGER,
PROCESADOR VARCHAR(50),
RESOLUCION INTEGER,
PANTALLA NUMERIC(5,2),
MEMORIA INTEGER,
MEMORIA_CHIP INTEGER,
TIPO_DE_MEMORIA VARCHAR(50),
POTENCIA INTEGER,
PRECIO_PRODUCTO NUMERIC(8,2) NOT NULL
);

CREATE TABLE PERSONA(
CORREO VARCHAR(20) PRIMARY KEY,
USUARIO VARCHAR(20) NOT NULL,
CONTRA VARCHAR(20) NOT NULL,
NOMBRE_USUARIO VARCHAR(20) NOT NULL,
APELLIDO VARCHAR(20) NOT NULL,
ROL varchar(10) NOT NULL
);

CREATE TABLE COMPROBANTE(
ID_COMPROBANTE INTEGER NOT NULL PRIMARY KEY,
NOMBRE VARCHAR(20) NOT NULL,
TOTAL NUMERIC(7,2) NOT NULL,
TIPO_COMPROBANTE VARCHAR(7) NOT NULL,
RUC NUMERIC(16)
);

INSERT INTO COMPROBANTE VALUES (100,'Marco',1200,'BOLETA',null);
INSERT INTO COMPROBANTE VALUES (200,'Arnold',1530,'BOLETA',null);
INSERT INTO COMPROBANTE VALUES (300,'Johan',1200,'BOLETA',null);

CREATE TABLE REGISTRO(
COD_PRODUCTO INTEGER NOT NULL,
CANT_PRODUCTO INTEGER NOT NULL,
ID_COMPROBANTE INTEGER NOT NULL,
FOREIGN KEY (COD_PRODUCTO) REFERENCES PRODUCTO (COD_PRODUCTO),
FOREIGN KEY (ID_COMPROBANTE) REFERENCES COMPROBANTE (ID_COMPROBANTE)
);

INSERT INTO REGISTRO VALUES (100,1,100);
INSERT INTO REGISTRO VALUES (200,1,100);
INSERT INTO REGISTRO VALUES (500,1,200);
INSERT INTO REGISTRO VALUES (900,1,200);
INSERT INTO REGISTRO VALUES (400,1,300);
INSERT INTO REGISTRO VALUES (800,1,300);

INSERT INTO PRODUCTO VALUES(100,'MOTO X', 'CELULAR', 'MOTOROLA', 100, NULL, NULL,
4, 'SNAP DRAGON', 24, 4, 15, 64, NULL, NULL, 600);------
INSERT INTO PRODUCTO VALUES(400,'MOTO Z', 'CELULAR', 'MOTOROLA', 120, NULL, NULL,
4, 'KIRIN 710', 24, 4, 15, 64, NULL, NULL, 700);
INSERT INTO PRODUCTO VALUES(500,'MOTO Y', 'CELULAR', 'MOTOROLA', 100, NULL, NULL,
4, 'MEDIA TECH', 24, 4, 15, 64, NULL, NULL, 800);

INSERT INTO PRODUCTO VALUES(200,'MODELO 5', 'LAPTOP', 'LENOVO', 400, NULL, NULL,
4, 'BTX', 42, 16, 1000, NULL, 'DISCO DURO', NULL, 600);-----------
INSERT INTO PRODUCTO VALUES(600,'MODELO 4', 'LAPTOP', 'HP', 200, NULL, NULL,
4, 'RAZING 5 3400G', 42, 16, 1000, NULL, 'DISCO DURO', NULL, 800);
INSERT INTO PRODUCTO VALUES(700,'MODELO 3', 'LAPTOP', 'LENOVO', 900, NULL, NULL,
4, 'RAZING 4500G', 42, 16, 1000, NULL, 'DISCO DURO', NULL, 1200);

INSERT INTO PRODUCTO VALUES(300,'MODELO A', 'LAVADORA', 'SAMSUNG', 250, 20, 3,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 600);
INSERT INTO PRODUCTO VALUES(800,'MODELO B', 'LAVADORA', 'LG', 450, 20, 3,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500);
INSERT INTO PRODUCTO VALUES(900,'MODELO C', 'LAVADORA', 'SAMSUNG', 650, 20, 3,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 730);

INSERT INTO PRODUCTO VALUES(1000,'MODELO A', 'MICROONDAS', 'SAMSUNG', 250, NULL, 3,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, 110, 600);
INSERT INTO PRODUCTO VALUES(1100,'MODELO B', 'MICROONDAS', 'LG', 450, NULL, 7,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, 500);
INSERT INTO PRODUCTO VALUES(1200,'MODELO C', 'MICROONDAS', 'SAMSUNG', 650, NULL, 5,
NULL, NULL, NULL, NULL, NULL, NULL, NULL, 220, 400);


INSERT INTO PERSONA VALUES ('jospw@gmail.com','Josh','1234','Jose','Poma','GERENTE');
INSERT INTO PERSONA VALUES ('FireSpirit@gmail.com','Jhon','911sep','Johan','Gaspar','GERENTE');
INSERT INTO PERSONA VALUES ('markillo@gmail.com','markillo','d4rk','Marco','Junior','GERENTE');
INSERT INTO PERSONA VALUES ('amor27@gmail.com','amor','987654321','Arnold','Centeno','GERENTE');

select * from PRODUCTO;

SELECT COD_PRODUCTO, NOM_PRODUCTO, TIPO, MARCA, CANTIDAD, PRECIO_PRODUCTO FROM PRODUCTO;

SELECT USUARIO, CONTRA FROM PERSONA WHERE USUARIO='markillo' AND CONTRA ='d4rk';
