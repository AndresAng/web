package uni.edu.poo.examenFinal.bean;

public class lavadora extends productos{
    private double capacidad;
    private String dimensiones;

    public lavadora(int codP, String nomP, String marcaP, double precio, int cant, double capacidad, String dimensiones) {
        super(codP, nomP, marcaP, precio, cant);
        this.capacidad = capacidad;
        this.dimensiones = dimensiones;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }

    public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }
}
