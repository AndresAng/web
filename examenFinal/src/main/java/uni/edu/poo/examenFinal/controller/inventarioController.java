package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uni.edu.poo.pc04.bean.dataInventario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

@RestController
public class inventarioController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/inventario")
    public ArrayList<dataInventario> inventario() throws Exception {
        ArrayList<dataInventario> D = new ArrayList<dataInventario>();
        Connection con = template.getDataSource().getConnection();
        String sql = "SELECT * FROM PRODUCTO";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            dataInventario DI = new dataInventario(rs.getInt(1), rs.getString(2),
                    rs.getString(3), rs.getString(4), rs.getInt(5),
                    rs.getFloat(16));
            D.add(DI);
        }
        return D;
    }
}
