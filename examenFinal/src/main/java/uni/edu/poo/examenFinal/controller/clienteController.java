package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uni.edu.poo.pc04.bean.comprobante;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

@RestController
public class clienteController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/clientesComprobantes")
    public  ArrayList<comprobante> mostrarFacturas() throws Exception{
        Connection con = template.getDataSource().getConnection();
        String sql = "SELECT * FROM COMPROBANTE";
        Statement ps2 = con.createStatement();
        ResultSet rs = ps2.executeQuery(sql);
        ArrayList<comprobante> ListaComp = new ArrayList<>();
        while(rs.next()){
            comprobante fn = new comprobante(rs.getInt(1),rs.getString(2),rs.getFloat(3),rs.getString(4),rs.getString(5));
            ListaComp.add(fn);
        }
        return ListaComp;
    }
}
