package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uni.edu.poo.pc04.bean.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RestController
public class inspeccionarProducto {

    @Autowired
    JdbcTemplate template;
    @GetMapping("/inspeccionar")
    public productoGeneral inspeccionar(@RequestParam Integer codigoProducto) throws Exception{
        String tipo=null, v="vacio";
        productoGeneral product = null;
        Connection con = template.getDataSource().getConnection();
        PreparedStatement st = con.prepareStatement("SELECT TIPO FROM PRODUCTO WHERE COD_PRODUCTO = ?");
        st.setInt(1,codigoProducto);
        ResultSet rs = st.executeQuery();
        while (rs.next()){
            tipo = rs.getString(1);
        }
        String sql=null;
        if(tipo.equals("CELULAR")){
            sql="SELECT COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,RAM,PROCESADOR,RESOLUCION" +
                    ",PANTALLA,MEMORIA,MEMORIA_CHIP,PRECIO_PRODUCTO FROM PRODUCTO WHERE COD_PRODUCTO = ?";
        }else if(tipo.equals("LAVADORA")){
            sql="SELECT COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,CAPACIDAD,DIMENSIONES" +
                    ",PRECIO_PRODUCTO FROM PRODUCTO WHERE COD_PRODUCTO = ?";
        }else if(tipo.equals("MICROONDAS")){
            sql="SELECT COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,DIMENSIONES" +
                    ",POTENCIA,PRECIO_PRODUCTO FROM PRODUCTO WHERE COD_PRODUCTO = ?";
        }else if(tipo.equals("LAPTOP")){
            sql="SELECT COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,RAM,PROCESADOR,RESOLUCION" +
                    ",PANTALLA,MEMORIA,TIPO_DE_MEMORIA,PRECIO_PRODUCTO FROM PRODUCTO WHERE COD_PRODUCTO = ?";
        }
        PreparedStatement st1=con.prepareStatement(sql);
        st1.setInt(1,codigoProducto);
        ResultSet rs1 = st1.executeQuery();
        if(tipo.equals("CELULAR")){
            while(rs1.next()){
                product = new productoGeneral(rs1.getInt(1),rs1.getString(2),rs1.getString(3),
                        rs1.getString(4),rs1.getInt(5),null,null,rs1.getInt(6),
                        rs1.getString(7),rs1.getInt(8),rs1.getDouble(9),rs1.getInt(10),
                        rs1.getInt(11),null,null,rs1.getDouble(12));
            }
            return product;
        }else if(tipo.equals("LAVADORA")){
            lavadora lav=null;
            while(rs1.next()){
                product = new productoGeneral(rs1.getInt(1),rs1.getString(2),rs1.getString(3),
                        rs1.getString(4),rs1.getInt(5),rs1.getDouble(6),rs1.getString(7),
                        null,null,null,null,null,null,null,null,rs1.getDouble(8));
            }
            return product;
        }else if(tipo.equals("MICROONDAS")){
            microondas mic=null;
            while(rs1.next()){
                product = new productoGeneral(rs1.getInt(1),rs1.getString(2),rs1.getString(3),
                        rs1.getString(4),rs1.getInt(5),null,rs1.getString(6),null,
                        null,null,null,null,null,null,rs1.getInt(7),rs1.getDouble(8));
            }
            return product;
        }else {
            laptop lap=null;
            while(rs1.next()){
                product = new productoGeneral(rs1.getInt(1),rs1.getString(2),rs1.getString(3),
                        rs1.getString(4),rs1.getInt(5),null,null,
                        rs1.getInt(6),rs1.getString(7),rs1.getInt(8),rs1.getDouble(9),
                        rs1.getInt(10),null,rs1.getString(11),null,rs1.getDouble(12));
            }
            return product;
        }
    }
}
