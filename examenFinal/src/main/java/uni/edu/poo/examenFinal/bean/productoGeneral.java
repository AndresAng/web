package uni.edu.poo.examenFinal.bean;

public class productoGeneral {
    private Integer codigo;
    private String nombre;
    private String tipo;
    private String marca;
    private Integer cantidad;
    private Double capacidad;
    private String dimensiones;
    private Integer ram;
    private String procesador;
    private Integer resolucion;
    private Double pantalla;
    private Integer memoria;
    private Integer memoriaChip;
    private String tipoMemoria;
    private Integer potencia;
    private Double precio;

    public productoGeneral(Integer codigo, String nombre, String tipo, String marca,
                           Integer cantidad, Double capacidad, String dimensiones, Integer ram,
                           String procesador, Integer resolucion, Double pantalla,
                           Integer memoria, Integer memoriaChip, String tipoMemoria,
                           Integer potencia, Double precio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tipo = tipo;
        this.marca = marca;
        this.cantidad = cantidad;
        this.capacidad = capacidad;
        this.dimensiones = dimensiones;
        this.ram = ram;
        this.procesador = procesador;
        this.resolucion = resolucion;
        this.pantalla = pantalla;
        this.memoria = memoria;
        this.memoriaChip = memoriaChip;
        this.tipoMemoria = tipoMemoria;
        this.potencia = potencia;
        this.precio = precio;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public Integer getResolucion() {
        return resolucion;
    }

    public void setResolucion(Integer resolucion) {
        this.resolucion = resolucion;
    }

    public Double getPantalla() {
        return pantalla;
    }

    public void setPantalla(Double pantalla) {
        this.pantalla = pantalla;
    }

    public Integer getMemoria() {
        return memoria;
    }

    public void setMemoria(Integer memoria) {
        this.memoria = memoria;
    }

    public Integer getMemoriaChip() {
        return memoriaChip;
    }

    public void setMemoriaChip(Integer memoriaChip) {
        this.memoriaChip = memoriaChip;
    }

    public String getTipoMemoria() {
        return tipoMemoria;
    }

    public void setTipoMemoria(String tipoMemoria) {
        this.tipoMemoria = tipoMemoria;
    }

    public Integer getPotencia() {
        return potencia;
    }

    public void setPotencia(Integer potencia) {
        this.potencia = potencia;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}