package uni.edu.poo.examenFinal.bean;

public class comprobante {
    Integer id_comprobante;
    String nombre;
    float total;
    String tipo_comprobante;
    String ruc;

    public comprobante(Integer id_comprobante, String nombre, float total, String tipo_comprobante, String ruc) {
        this.id_comprobante = id_comprobante;
        this.nombre = nombre;
        this.total = total;
        this.tipo_comprobante = tipo_comprobante;
        this.ruc = ruc;
    }

    public Integer getId_comprobante() {
        return id_comprobante;
    }

    public void setId_comprobante(Integer id_comprobante) {
        this.id_comprobante = id_comprobante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getTipo_comprobante() {
        return tipo_comprobante;
    }

    public void setTipo_comprobante(String tipo_comprobante) {
        this.tipo_comprobante = tipo_comprobante;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
}
