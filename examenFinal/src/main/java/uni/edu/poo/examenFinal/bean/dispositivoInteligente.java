package uni.edu.poo.examenFinal.bean;

public abstract class dispositivoInteligente extends productos{
    protected Integer ram;
    protected String procesador;
    protected Integer resol;
    protected Double pantalla;
    protected Integer memoria;

    public dispositivoInteligente(int codP, String nomP, String marcaP, double precio, int cant, Integer ram, String procesador, Integer resol, Double pantalla, Integer memoria) {
        super(codP, nomP, marcaP, precio, cant);
        this.ram = ram;
        this.procesador = procesador;
        this.resol = resol;
        this.pantalla = pantalla;
        this.memoria = memoria;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public Integer getResol() {
        return resol;
    }

    public void setResol(Integer resol) {
        this.resol = resol;
    }

    public Double getPantalla() {
        return pantalla;
    }

    public void setPantalla(Double pantalla) {
        this.pantalla = pantalla;
    }

    public Integer getMemoria() {
        return memoria;
    }

    public void setMemoria(Integer memoria) {
        this.memoria = memoria;
    }
}
