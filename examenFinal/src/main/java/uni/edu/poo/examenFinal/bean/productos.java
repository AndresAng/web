package uni.edu.poo.examenFinal.bean;

public abstract class productos {
    protected int codP;
    protected String nomP;
    protected String marcaP;
    protected double precio;
    protected int cant;

    public productos(int codP, String nomP, String marcaP, double precio, int cant) {
        this.codP = codP;
        this.nomP = nomP;
        this.marcaP = marcaP;
        this.precio = precio;
        this.cant = cant;
    }

    public int getCodP() {
        return codP;
    }

    public void setCodP(int codP) {
        this.codP = codP;
    }

    public String getNomP() {
        return nomP;
    }

    public void setNomP(String nomP) {
        this.nomP = nomP;
    }

    public String getMarcaP() {
        return marcaP;
    }

    public void setMarcaP(String marcaP) {
        this.marcaP = marcaP;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }
}
