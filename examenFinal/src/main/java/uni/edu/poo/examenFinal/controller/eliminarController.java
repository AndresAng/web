package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Controller
public class eliminarController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/eliminarProducto")
    public String eliminar(@RequestParam Integer codigoProducto,
                           @RequestParam Integer cantidad) throws Exception{
        Connection con1 = template.getDataSource().getConnection();
        String sql1= "SELECT (CANTIDAD) FROM PRODUCTO WHERE COD_PRODUCTO = ?";
        PreparedStatement ps2 = con1.prepareStatement(sql1);
        ps2.setInt(1,codigoProducto);
        ResultSet rs = ps2.executeQuery();
        rs.next();
        Integer base = rs.getInt(1);
        if(base > cantidad){
            String sql2 = "UPDATE PRODUCTO SET CANTIDAD = ? WHERE COD_PRODUCTO = ?";
            Connection con2 = template.getDataSource().getConnection();
            PreparedStatement ps = con2.prepareStatement(sql2);
            ps.setInt(1, base - cantidad);
            ps.setInt(2, codigoProducto);
            ps.executeUpdate();
            return "redirect:/inventario.html";
        }else{
            String sql3 = "DELETE FROM PRODUCTO WHERE COD_PRODUCTO = ?";
            Connection con3 = template.getDataSource().getConnection();
            PreparedStatement ps = con3.prepareStatement(sql3);
            ps.setInt(1, codigoProducto);
            ps.executeUpdate();
            String error = "La cantidad ingresada supero la que se tiene en almacen. Tu producto fue eliminado";
            return error;
        }
    }
}
