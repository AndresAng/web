package uni.edu.poo.examenFinal.bean;

public class microondas extends productos{
    private int potencia;
    private double capacidad;

    public microondas(int codP, String nomP, String marcaP, double precio, int cant, int potencia, double capacidad) {
        super(codP, nomP, marcaP, precio, cant);
        this.potencia = potencia;
        this.capacidad = capacidad;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }
}
