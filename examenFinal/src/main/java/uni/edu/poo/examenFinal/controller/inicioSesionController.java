package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Controller
public class inicioSesionController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/ingresoUsuario")
    public String ingresoUsuario(@RequestParam String usuario, @RequestParam String contrasena) throws Exception{
        Connection con = template.getDataSource().getConnection();
        String sql3 = "SELECT USUARIO, CONTRA FROM PERSONA WHERE USUARIO=? AND CONTRA = ?";
        PreparedStatement ps2 = con.prepareStatement(sql3);
        ps2.setString(1,usuario);
        ps2.setString(2,contrasena);
        ResultSet rs = ps2.executeQuery();
        if(rs.next()){
            return "redirect:/home.html";
        }else{
            return "redirect:/index.html";
        }
    }
}
