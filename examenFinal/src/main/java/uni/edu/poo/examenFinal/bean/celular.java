package uni.edu.poo.examenFinal.bean;

public class celular extends dispositivoInteligente{
    private Integer chipMemoria;

    public celular(int codP, String nomP, String marcaP, double precio, int cant, Integer ram, String procesador, Integer resol, Double pantalla, Integer memoria, Integer chipMemoria) {
        super(codP, nomP, marcaP, precio, cant, ram, procesador, resol, pantalla, memoria);
        this.chipMemoria = chipMemoria;
    }

    public Integer getChipMemoria() {
        return chipMemoria;
    }

    public void setChipMemoria(Integer chipMemoria) {
        this.chipMemoria = chipMemoria;
    }
}
