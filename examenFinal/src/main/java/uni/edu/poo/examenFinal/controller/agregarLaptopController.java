package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;

@Controller
public class agregarLaptopController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/agregarLaptop")
    public String agregarProducto(@RequestParam Integer codProd, @RequestParam String nomProd,
                                  @RequestParam String marca,
                                  @RequestParam Integer cantidad, @RequestParam Integer ram,
                                  @RequestParam String procesador, @RequestParam Integer resolucion,
                                  @RequestParam Double pantalla, @RequestParam Integer memoria,
                                  @RequestParam String tipoMemoria, @RequestParam Double precioProd
                                  ) throws Exception{
        Connection con = template.getDataSource().getConnection();
        String sql = "INSERT INTO PRODUCTO(COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,RAM,PROCESADOR,RESOLUCION,PANTALLA,MEMORIA,TIPO_DE_MEMORIA,PRECIO_PRODUCTO) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pst = con.prepareStatement(sql);
        pst.setInt(1,codProd);
        pst.setString(2,nomProd);
        pst.setString(3,"LAPTOP");
        pst.setString(4,marca);
        pst.setInt(5,cantidad);
        pst.setInt(6,ram);
        pst.setString(7,procesador);
        pst.setInt(8,resolucion);
        pst.setDouble(9,pantalla);
        pst.setInt(10,memoria);
        pst.setString(11,tipoMemoria);
        pst.setDouble(12,precioProd);
        pst.executeUpdate();
        return "home.html";
    }
}
