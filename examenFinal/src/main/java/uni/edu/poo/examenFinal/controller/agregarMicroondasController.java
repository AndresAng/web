package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;

@Controller
public class agregarMicroondasController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping("/agregarMicroondas")
    public String agregarProducto(@RequestParam Integer codProd, @RequestParam String nomProd,
                                  @RequestParam String marca,
                                  @RequestParam Integer cantidad,
                                  @RequestParam String dimensiones,
                                  @RequestParam Integer potencia, @RequestParam Double precioProd
    ) throws Exception{
        Connection con = template.getDataSource().getConnection();
        String sql = "INSERT INTO PRODUCTO(COD_PRODUCTO,NOM_PRODUCTO,TIPO,MARCA,CANTIDAD,DIMENSIONES,POTENCIA,PRECIO_PRODUCTO) VALUES(?,?,?,?,?,?,?,?)";
        PreparedStatement pst = con.prepareStatement(sql);
        pst.setInt(1,codProd);
        pst.setString(2,nomProd);
        pst.setString(3,"MICROONDAS");
        pst.setString(4,marca);
        pst.setInt(5,cantidad);
        pst.setString(6,dimensiones);
        pst.setInt(7,potencia);
        pst.setDouble(8,precioProd);
        pst.executeUpdate();
        return "home.html";
    }
}
