package uni.edu.poo.examenFinal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uni.edu.poo.pc04.bean.dataProducto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@RestController
public class productosTopController {

    @Autowired
    JdbcTemplate template;

    @RequestMapping("/productosTop")
    public ArrayList<dataProducto> productoTop() throws Exception{
        Double celular=0.0,laptop=0.0,lavadora=0.0,microondas=0.0,precio;
        Connection con=template.getDataSource().getConnection();
        PreparedStatement st=con.prepareStatement("SELECT P.TIPO,R.CANT_PRODUCTO,P.PRECIO_PRODUCTO " +
                "FROM PRODUCTO P JOIN REGISTRO R ON P.COD_PRODUCTO = R.COD_PRODUCTO WHERE P.TIPO = ?");
        st.setString(1,"CELULAR");
        ResultSet rs=st.executeQuery();
        while (rs.next()){
            precio=rs.getDouble(3)*rs.getInt(2);
            celular+=precio;
        }
        PreparedStatement st1=con.prepareStatement("SELECT P.TIPO,R.CANT_PRODUCTO,P.PRECIO_PRODUCTO " +
                "FROM PRODUCTO P JOIN REGISTRO R ON P.COD_PRODUCTO = R.COD_PRODUCTO WHERE P.TIPO = ?");
        st1.setString(1,"LAPTOP");
        ResultSet rs1=st1.executeQuery();
        while (rs1.next()){
            precio=rs1.getDouble(3)*rs1.getInt(2);
            laptop+=precio;
        }
        PreparedStatement st2=con.prepareStatement("SELECT P.TIPO,R.CANT_PRODUCTO,P.PRECIO_PRODUCTO " +
                "FROM PRODUCTO P JOIN REGISTRO R ON P.COD_PRODUCTO = R.COD_PRODUCTO WHERE P.TIPO = ?");
        st2.setString(1,"LAVADORA");
        ResultSet rs2=st2.executeQuery();
        while (rs2.next()){
            precio=rs2.getDouble(3)*rs2.getInt(2);
            lavadora+=precio;
        }
        PreparedStatement st3=con.prepareStatement("SELECT P.TIPO,R.CANT_PRODUCTO,P.PRECIO_PRODUCTO " +
                "FROM PRODUCTO P JOIN REGISTRO R ON P.COD_PRODUCTO = R.COD_PRODUCTO WHERE P.TIPO = ?");
        st3.setString(1,"MICROONDAS");
        ResultSet rs3=st3.executeQuery();
        while (rs3.next()){
            precio=rs3.getDouble(3)*rs3.getInt(2);
            microondas+=precio;
        }

        dataProducto d1=new dataProducto("Celular",celular);
        dataProducto d2=new dataProducto("Laptop",laptop);
        dataProducto d3=new dataProducto("Lavadora",lavadora);
        dataProducto d4=new dataProducto("Microondas",microondas);

        ArrayList<dataProducto> lista = new ArrayList<>();
        lista.add(d1);
        lista.add(d2);
        lista.add(d3);
        lista.add(d4);

        dataProducto aux=new dataProducto(null,null);

        for(int i=0;i<4;i++){
            for(int j=i+1;j<4;j++){
                if(lista.get(i).getPrecio()<lista.get(j).getPrecio()){
                    aux.setPrecio(lista.get(j).getPrecio());
                    aux.setNombre(lista.get(j).getNombre());
                    lista.get(j).setNombre(lista.get(i).getNombre());
                    lista.get(j).setPrecio(lista.get(i).getPrecio());
                    lista.get(i).setNombre(aux.getNombre());
                    lista.get(i).setPrecio(aux.getPrecio());
                }
            }
        }
        return lista;
    }
}
