package uni.edu.poo.examenFinal.bean;

public class laptop extends dispositivoInteligente{
    private String tipoMemoria;

    public laptop(int codP, String nomP, String marcaP, double precio, int cant, Integer ram, String procesador, Integer resol, Double pantalla, Integer memoria, String tipoMemoria) {
        super(codP, nomP, marcaP, precio, cant, ram, procesador, resol, pantalla, memoria);
        this.tipoMemoria = tipoMemoria;
    }

    public String getTipoMemoria() {
        return tipoMemoria;
    }

    public void setTipoMemoria(String tipoMemoria) {
        this.tipoMemoria = tipoMemoria;
    }
}
